package org.bitbucket.igelborstel.igelutils;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WhitelistkickCommand implements CommandExecutor
{

	private IgelUtils mainPlugin;
	WhitelistkickCommand(IgelUtils m)
	{
		this.mainPlugin = m;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("whitelistkick"))
		{
			if(args.length > 0)
			{
				return false;
			}
			if(sender.hasPermission("igelutils.whitelistkick") || sender.isOp())
			{
				this.mainPlugin.getServer().setWhitelist(true);
				Set<OfflinePlayer> whitelistedPlayers = this.mainPlugin.getServer().getWhitelistedPlayers();
				for(Player player : this.mainPlugin.getServer().getOnlinePlayers())
				{
					if(whitelistedPlayers.contains((OfflinePlayer) player))
					{
						continue;
					}
					else if(player.isOp() || player.hasPermission("igelutils.whitelistkick.ignore"))
					{
						continue;
					}
					else
					{
						player.kickPlayer("Du wurdest vom Server geworfen, weil du nicht gewhitelisted bist");
					}
				}
				this.mainPlugin.getServer().broadcastMessage(ChatColor.DARK_BLUE + "[" + ChatColor.BLUE + "IgelUtils" + ChatColor.DARK_BLUE + "] " + ChatColor.WHITE + " Alle Spieler die nicht auf der Whitelist standen, wurden vom Server geworfen.");
			}
		}
		return false;
	}
}
