package org.bitbucket.igelborstel.igelutils;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.DisplaySlot;

/**
 * Created by Markus on 30.11.2015.
 */
public class Informationboard implements Runnable
{
    private IgelUtils plugin;
    private Scoreboard scoreBoard;
    private Objective primaryObjective;
    private int time = 0;

    Informationboard(IgelUtils p)
    {
        this.plugin = p;
        this.scoreBoard = this.plugin.getServer().getScoreboardManager().getNewScoreboard();
        this.primaryObjective = this.scoreBoard.registerNewObjective("Stats", "stats");
        this.primaryObjective.setDisplayName("IGS Server");
        this.primaryObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }
    public void run() {
        for(Player p : this.plugin.getServer().getOnlinePlayers())
        {
            p.setScoreboard(scoreBoard);
        }
        this.time++;
        //this.primaryObjective.getScore("Uptime: ").setScore(this.time / 60 / 60);
        this.primaryObjective.getScore("§2Team: ").setScore(getTeamMembers());
        this.primaryObjective.getScore("§bMember: ").setScore(this.plugin.getServer().getOnlinePlayers().size() - getTeamMembers());
    }

    public int getTeamMembers() {
        int teamMembers = 0;
        for (Player p : this.plugin.getServer().getOnlinePlayers()) {
            if (p.hasPermission("igelutils.mod")) {
                teamMembers++;
            }
        }
        return teamMembers;
    }
}