package org.bitbucket.igelborstel.igelutils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;

public class BroadcastManager 
{
	private IgelUtils mainPlugin;
	private ArrayList<Integer> tasks;
	private HashMap<Integer, Integer> ids;
	private HashMap<Integer, Integer> intervalls;
	private HashMap<Integer, String> messages;
	
	public BroadcastManager(IgelUtils m)
	{
		this.mainPlugin = m;
		tasks = new ArrayList<Integer>();
		this.ids = new HashMap<Integer, Integer>();
		this.intervalls = new HashMap<Integer, Integer>();
		this.messages = new HashMap<Integer, String>();
		loadBroadcasts();
	}
	private void loadBroadcasts() 
	{
		
		if(this.mainPlugin.database.isConnected())
		{
			ResultSet results = mainPlugin.database.doQuery("SELECT id, intervall, message, activated FROM `igelutils_broadcasts` ORDER BY priority");
			try 
			{
				int i = 0;
				while(results.next())
				{
					
					int id = results.getInt("id");
					int intervall = results.getInt("intervall");
					String message = results.getString("message");
					if(!results.getBoolean("activated"))
					{
						continue;
					}
					this.ids.put(new Integer(i++), new Integer(id));
					this.intervalls.put(new Integer(id), new Integer(intervall));
					this.messages.put(new Integer(id), message);
				}
			} catch (SQLException e) 
			{
				System.err.println(ChatColor.translateAlternateColorCodes('&', mainPlugin.getConfig().getString("broadcaster.prefix")) + "Fehler bei der Auswertung des Resultsets beim Laden der Broadcasts!");
				e.printStackTrace();
				return;
			}
			for(int i = 0; i < ids.size(); ++i)
			{
				int id = this.ids.get(new Integer(i)).intValue();
				int intervall = this.intervalls.get(new Integer(id)).intValue();
				final String msg = this.messages.get(new Integer(id));
				tasks.add(new Integer(mainPlugin.getServer().getScheduler().scheduleSyncRepeatingTask(mainPlugin, new Runnable() {
	
					@Override
					public void run() {
						mainPlugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', mainPlugin.getConfig().getString("broadcaster.prefix")) + ChatColor.translateAlternateColorCodes('&', msg));
					}}, 0L, intervall * 1200L)));
			}
		}
	}
	
	public void reloadBroadcasts()
	{
		killTasks();
		clearData();
		loadBroadcasts();
	}
	private void clearData() 
	{
		this.tasks.clear();
		this.ids.clear();
		this.intervalls.clear();
		this.messages.clear();
	}
	private void killTasks() 
	{
		for(Integer taskid : this.tasks)
		{
			mainPlugin.getServer().getScheduler().cancelTask(taskid);
		}
	}
}
