package org.bitbucket.igelborstel.igelutils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BroadcastCommand implements CommandExecutor
{
	private IgelUtils m;
	
	public BroadcastCommand(IgelUtils m)
	{
		this.m = m;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("broadcast"))
		{
			if(sender.hasPermission("igelutils.broadcast"))
			{
				StringBuilder bcMessage = new StringBuilder();
				for(String s : args)
				{
					bcMessage.append(s);
					bcMessage.append(' ');
				}
				String s = ChatColor.translateAlternateColorCodes('&', bcMessage.toString());
				m.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', m.getConfig().getString("broadcast.prefix")) + s);
				return true;
			}
			return false;
		}
		return false;
	}

}
