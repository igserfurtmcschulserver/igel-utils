package org.bitbucket.igelborstel.igelutils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BroadcasterCommands implements CommandExecutor
{
	private IgelUtils mainPlugin;
	static int msgids;
	
	public BroadcasterCommands(IgelUtils m)
	{
		this.mainPlugin = m;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(args.length < 1)
		{
			return false;
		}
		if(cmd.getName().equalsIgnoreCase("broadcaster"))
		{
			if(args[0].equalsIgnoreCase("new"))
			{
				if(args.length < 3)
				{
					sender.sendMessage("Zu wenig Argumente f�r einen neuen Broadcast angegeben");
					sender.sendMessage("Format: /broadcaster new [zeit] [message]");
					return true;
				}
				if(sender.hasPermission("igelutils.broadcaster.new"))
				{
					int intervall;
					try
					{
						intervall = Integer.parseInt(args[1]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das dritte Argument muss eine Zahl sein!");
						sender.sendMessage("Format: /broadcaster new [zeit] [message]");
						return true;
					}
					if(intervall > 120)
					{
						sender.sendMessage("Der Intervall darf maximal 120 Minuten betragen!");
						sender.sendMessage("Format: /broadcaster new [zeit] [message]");
						return true;
					}
					else if(intervall < 1)
					{
						sender.sendMessage("Der Intervall muss mindestens 1 Minute betragen!");
						sender.sendMessage("Format: /broadcaster new [zeit] [message]");
						return true;
					}
					StringBuilder msgBuilder = new StringBuilder();
					for(int i = 0; i < args.length; ++i)
					{
						if(i < 2)
						{
							continue;
						}
						msgBuilder.append(args[i]);
						msgBuilder.append(' ');
					}
					String msg = msgBuilder.toString();
					if(msg.length() > 200)
					{
						sender.sendMessage("Die Nachricht darf maximal 200 Zeichen lang sein");
						sender.sendMessage("Format: /broadcaster new [zeit] [message]");
						return true;
					}
					mainPlugin.database.doUpdate("INSERT INTO `igelutils_broadcasts`( `intervall`, `message`, `activated`) VALUES (" + intervall + ",\"" + msg + "\", " + true + ")");
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', mainPlugin.getConfig().getString("broadcaster.prefix")) + "Der Broadcast " + ChatColor.translateAlternateColorCodes('&', msg) + ChatColor.RESET + " wurde mit dem Intervall " + intervall + " erstellt!"); 
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("list"))
			{
				if(args.length > 1)
				{
					return false;
				}
				if(sender.hasPermission("igelutils.broadcaster.list"))
				{
					ResultSet results = mainPlugin.database.doQuery("SELECT id, intervall, message, activated, priority FROM `igelutils_broadcasts`");
					HashMap<Integer, Integer> ids = new HashMap<Integer, Integer>();
					HashMap<Integer, Integer> intervalls = new HashMap<Integer, Integer>();
					HashMap<Integer, String> messages = new HashMap<Integer, String>();
					HashMap<Integer, Boolean> activated = new HashMap<Integer, Boolean>();
					HashMap<Integer, Integer> prioritys = new HashMap<Integer, Integer>();
					try 
					{
						int i = 0;
						while(results.next())
						{
							
							int id = results.getInt("id");
							int intervall = results.getInt("intervall");
							String message = results.getString("message");
							boolean active = results.getBoolean("activated");
							int prio = results.getInt("priority");
							ids.put(new Integer(i++), new Integer(id));
							intervalls.put(new Integer(id), new Integer(intervall));
							messages.put(new Integer(id), message);
							activated.put(new Integer(id), new Boolean(active));
							prioritys.put(new Integer(id), new Integer(prio));
						}
					} catch (SQLException e) 
					{
						System.err.println(ChatColor.translateAlternateColorCodes('&', mainPlugin.getConfig().getString("broadcaster.prefix")) + "Fehler bei der Auswertung des Resultsets bei /broadcaster list!");
						e.printStackTrace();
						return true;
					}
					sender.sendMessage("Folgende Broadcasts sind in der Datenbank eingeschrieben:");
					sender.sendMessage("Format: [id] [Intervall in Minuten] [Text] [Activated] [Priorit�t]");
					for(int i = 0; i < intervalls.size(); ++i)
					{
						int id = ids.get(new Integer(i)).intValue();
						int intervall = intervalls.get(new Integer(id)).intValue();
						String msg = messages.get(new Integer(id));
						boolean active = activated.get(new Integer(id));
						int prio = prioritys.get(new Integer(id));
						sender.sendMessage(id + ") [" + intervall + "] " + ChatColor.translateAlternateColorCodes('&', msg) + ChatColor.RESET + " (" + active + ")" + " [" + prio + "]");
					}
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("delete"))
			{
				if(args.length > 2 || args.length < 2)
				{
					return false;
				}
				if(sender.hasPermission("igelutils.broadcaster.delete"))
				{
					int id;
					try
					{
						id = Integer.parseInt(args[1]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
						sender.sendMessage("Format: /broadcaster delete [id]");
						return true;
					}
					sender.sendMessage("Der Broadcast mit der Id " + id + ") wird aus der Datenbank gel�scht.");
					mainPlugin.database.doUpdate("DELETE FROM `igelutils_broadcasts` WHERE id=" + id);
					sender.sendMessage("Der Broadcast wurde erfolgreich aus der Datenbank gel�scht! Damit dies im Spiel sichtbar wird, muss der Broadcaster neugeladen werden!");
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("reload"))
			{
				if(args.length > 1)
				{
					return false;
				}
				if(sender.hasPermission("igelutils.broadcaster.reload"))
				{
					mainPlugin.broadcaster.reloadBroadcasts();
					sender.sendMessage("Alle Broadcasts neu geladen!");
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("edittext"))
			{
				if(args.length < 3)
				{
					sender.sendMessage("Zu wenig Argumente angegeben!");
					sender.sendMessage("Format: /broadcaster edittext [id] [message]");
					return true;
				}
				if(sender.hasPermission("igelutils.broadcaster.edit.text"))
				{
					int id;
					try
					{
						id = Integer.parseInt(args[1]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
						sender.sendMessage("Format: /broadcaster edittext [id] [message]");
						return true;
					}
					StringBuilder msgBuilder = new StringBuilder();
					for(int i = 0; i < args.length; ++i)
					{
						if(i < 2)
						{
							continue;
						}
						msgBuilder.append(args[i]);
						msgBuilder.append(' ');
					}
					String msg = msgBuilder.toString();
					if(msg.length() > 200)
					{
						sender.sendMessage("Die Nachricht darf maximal 200 Zeichen lang sein");
						sender.sendMessage("Format: /broadcaster edittext [id] [message]");
						return true;
					}
					mainPlugin.database.doUpdate("UPDATE `igelutils_broadcasts` SET `message` = \'" + msg + "\' WHERE `id` = " + id);
					sender.sendMessage("Der Broadcast mit der ID " + id + " wurde erfolgreich bearbeitet! Damit dieser aktiv wird musst du den Broadcaster neu laden!");
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("toggle"))
			{
				if(args.length < 2)
				{
					sender.sendMessage("Zu wenig Argumente angegeben!");
					sender.sendMessage("Format: /broadcaster toggle [id]");
					return false;
				}
				if(sender.hasPermission("igelutils.broadcaster.toggle"))
				{
					int id;
					try
					{
						id = Integer.parseInt(args[1]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
						sender.sendMessage("Format: /broadcaster toggle [id]");
						return true;
					}
					ResultSet results = mainPlugin.database.doQuery("SELECT activated FROM `igelutils_broadcasts` WHERE `id` = " + id);
					boolean stat;
					try 
					{
						if(results.next())
						{
							stat = results.getBoolean("activated");
						}
						else
						{
							stat = false;
						}
					} catch (SQLException e) {
						System.err.println(ChatColor.translateAlternateColorCodes('&', mainPlugin.getConfig().getString("broadcaster.prefix")) + "Fehler bei der Abfrage eines Status!");
						e.printStackTrace();
						return true;
					}
					sender.sendMessage("Der Status vom Broadcast " + id + " wurde von " + stat + " auf " + !stat + "ge�ndert");
					sender.sendMessage("Damit dies aktiv wird, m�ssen die Broadcasts neu geladen werden!");
					mainPlugin.database.doUpdate("UPDATE `igelutils_broadcasts` SET `activated` = " + !stat + " WHERE `id` = " + id);
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("setpriority"))
			{
				if(args.length != 3)
				{
					return false;
				}
				if(sender.hasPermission("igelutils.broadcaster.setpriority"))
				{
					int id;
					try
					{
						id = Integer.parseInt(args[1]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
						return true;
					}
					
					int priority;
					try
					{
						priority = Integer.parseInt(args[2]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das dritte Argument muss eine Zahl sein!");
						return true;
					}
					if(priority < 1)
					{
						sender.sendMessage("Die Priorit�t muss mindestens 1 betragen!");
						return true;
					}
					
					
					mainPlugin.database.doUpdate("UPDATE `igelutils_broadcasts` SET `priority` = " + priority + " WHERE `id` = " + id);
					sender.sendMessage("Die Priorit�t vom Broadcast " + id + " wurde auf " + priority + " gesetzt!");
					return true;
				}
			}
		}
		return false;
	}

}
