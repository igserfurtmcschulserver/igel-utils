package org.bitbucket.igelborstel.igelutils;

import org.bitbucket.igelborstel.igelcore.config.Config;
import org.bitbucket.igelborstel.igelcore.database.MySql;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.cagigs.igelutils.commands.AcceptCommand;
import de.cagigs.igelutils.commands.BroadcastCommand;
import de.cagigs.igelutils.commands.BroadcasterCommands;
import de.cagigs.igelutils.commands.IgelutilsCommand;
import de.cagigs.igelutils.commands.RulesacceptCommand;
import de.cagigs.igelutils.commands.WhitelistkickCommand;
import org.bitbucket.igelborstel.igelutils.Informationboard;

public class IgelUtils extends JavaPlugin
{
	public MySql database;
	public BroadcastManager broadcaster;
	public Config config;
	
	@Override
	@SuppressWarnings("Deprecated")
	public void onEnable()
	{
		Informationboard infoboard = new Informationboard(this);
		if(!this.getServer().getPluginManager().isPluginEnabled("IgelCore"))
		{
			System.err.println("[IgelUtils] Can not find [IgelCore]! Disable IgelUtils!");
			this.getServer().getPluginManager().disablePlugin(this);
		}
		database = new MySql(this.getDescription().getName());
		loadConfig();
		if(this.getConfig().getString("rulesaccept.lang").equals("en_En"))
		{
			config = new Config(this, "en_En", "lang");
			this.setEnglishDefault();
		}
		else if(this.getConfig().getString("rulesaccept.lang").equals("de_De"))
		{
			config = new Config(this, "de_De", "lang");
			this.setGermanDefault();
		}
		else
		{
			config = new Config(this, "de_De", "lang");
			this.setGermanDefault();
		}
		this.getCommand("broadcast").setExecutor(new BroadcastCommand(this));
		if(this.getConfig().getBoolean("broadcaster.enableBroadcaster"))
		{
			System.out.println("[IgelUtils] Broadcaster is enabled. Loading.");
			if(database.connect())
			{
				database.doUpdate("CREATE TABLE IF NOT EXISTS `igelutils_broadcasts` (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, intervall INT NOT NULL, message VARCHAR(200), activated BOOLEAN, priority INT DEFAULT '1000')");
			}
			this.getCommand("broadcaster").setExecutor(new BroadcasterCommands(this));
			broadcaster = new BroadcastManager(this);
		}
		this.getCommand("whitelistkick").setExecutor(new WhitelistkickCommand(this));
		this.getCommand("igelutils").setExecutor(new IgelutilsCommand(this));
		this.getCommand("accept").setExecutor(new AcceptCommand(this));
		this.getCommand("rulesaccept").setExecutor(new RulesacceptCommand(this));
		if(this.getConfig().getBoolean("scoreboard.enable"))
		{
			System.out.println("[IgelUtils] Enable scoreboard!");
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, infoboard, 0L, 20L);
		}
	}
	
	public void loadConfig() 
	{
		this.reloadConfig();
		this.getConfig().options().header("Configurationsdatei für IgelUtils Version: " + this.getDescription().getVersion());
		this.getConfig().addDefault("broadcaster.enableBroadcaster", false);
		this.getConfig().addDefault("broadcaster.prefix", "&1[&9IgelUtils&1] &r");
		this.getConfig().addDefault("broadcast.prefix", "&1[&9IgelUtils&1] &r");
		this.getConfig().addDefault("rulesaccept.password", "password");
		this.getConfig().addDefault("rulesaccept.newbiegroup", "default");
		this.getConfig().addDefault("rulesaccept.promoteGroup", "Spieler");
		this.getConfig().addDefault("rulesaccept.playertp.x", 100);
		this.getConfig().addDefault("rulesaccept.playertp.y", 100);
		this.getConfig().addDefault("rulesaccept.playertp.z", 100);
		this.getConfig().addDefault("rulesaccept.playertp.world", "world");
		this.getConfig().addDefault("rulesaccept.playertp.yaw", 1);
		this.getConfig().addDefault("rulesaccept.playertp.pitch", 1);
		this.getConfig().addDefault("rulesaccept.lang", "en_En");
		this.getConfig().addDefault("scoreboard.enable", false);
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}
	
	private void setEnglishDefault()
	{
		this.config.reloadConfig();
		this.config.getConfig().addDefault("lang.reload.start", "Reload Config");
		this.config.getConfig().addDefault("lang.reload.reloaded", "Reload complete");
		this.config.getConfig().addDefault("lang.console.warnings.accept", "Only players can Accept the rules");
		this.config.getConfig().addDefault("lang.console.warnings.tp", "Only players can teleport");
		this.config.getConfig().addDefault("lang.console.warnings.settp", "Only players can set the Teleportpoint");
		this.config.getConfig().addDefault("lang.manage.settp", "Telportpoint set!");
		this.config.getConfig().addDefault("lang.manage.tp", "Teleport to the acceptpoint");
		this.config.getConfig().addDefault("lang.accept.secondaccept", "You can accept the rules only once");
		this.config.getConfig().addDefault("lang.accept.wrongPassword", "is the wrong password");
		this.config.getConfig().addDefault("lang.accept.acceptBroadcast", " is now a player!");
		this.config.getConfig().addDefault("lang.accept.personalMessage", "You are now a player!");
		this.config.getConfig().options().copyDefaults(true);
		this.config.saveConfig();
	}
	private void setGermanDefault() 
	{
		this.config.reloadConfig();
		this.config.getConfig().addDefault("lang.reload.start", "Lade Konfigdatei neu...");
		this.config.getConfig().addDefault("lang.reload.reloaded", "Neuladen der Konfig abgeschlossen");
		this.config.getConfig().addDefault("lang.console.warnings.accept", "Nur Spieler koennen die Regeln akzeptieren!");
		this.config.getConfig().addDefault("lang.console.warnings.tp", "Nur Spieler koennen sich teleportieren");
		this.config.getConfig().addDefault("lang.console.warnings.settp", "Nur Spieler koennen den Teleportpunkt setzen");
		this.config.getConfig().addDefault("lang.manage.settp", "Teleportpunkt gesetzt!");
		this.config.getConfig().addDefault("lang.manage.tp", "Teleportiere zum Freischaltpunkt");
		this.config.getConfig().addDefault("lang.accept.secondaccept", "Du kannst dich nur einmal freischalten!");
		this.config.getConfig().addDefault("lang.accept.wrongPassword", " ist das falsche Passwort");
		this.config.getConfig().addDefault("lang.accept.acceptBroadcast", " &2wurde Erfolgreich zum Spieler!");
		this.config.getConfig().addDefault("lang.accept.personalMessage", "Du hast dich erfolgreich freigeschalten. Um dir ein Grundstück zu erstellen kannst du die Redstonechest einfach in der Welt platzieren.");
		this.config.getConfig().options().copyDefaults(true);
		this.config.saveConfig();
	}


	@Override
	public void onDisable()
	{
		if(database.isConnected())
		{
			database.close();
		}
	}
}
