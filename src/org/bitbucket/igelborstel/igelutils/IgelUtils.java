package org.bitbucket.igelborstel.igelutils;

import org.bitbucket.igelborstel.igelcore.database.MySql;
import org.bukkit.plugin.java.JavaPlugin;

public class IgelUtils extends JavaPlugin
{
	MySql database;
	BroadcastManager broadcaster;
	
	@Override
	public void onEnable()
	{
		if(!this.getServer().getPluginManager().isPluginEnabled("IgelCore"))
		{
			System.err.println("[IgelUtils] Can not find [IgelCore]! Disable IgelUtils!");
			this.getServer().getPluginManager().disablePlugin(this);
		}
		database = new MySql(this.getDescription().getName());
		loadConfig();
		this.getCommand("broadcast").setExecutor(new BroadcastCommand(this));
		if(this.getConfig().getBoolean("broadcaster.enableBroadcaster"))
		{
			System.out.println("[IgelUtils] Broadcaster is enabled. Loading.");
			if(database.connect())
			{
				database.doUpdate("CREATE TABLE IF NOT EXISTS `igelutils_broadcasts` (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, intervall INT NOT NULL, message VARCHAR(200), activated BOOLEAN, priority INT DEFAULT '1000')");
			}
			this.getCommand("broadcaster").setExecutor(new BroadcasterCommands(this));
			broadcaster = new BroadcastManager(this);
		}
		this.getCommand("whitelistkick").setExecutor(new WhitelistkickCommand(this));
	}
	
	public void loadConfig() 
	{
		this.reloadConfig();
		this.getConfig().options().header("Configurationsdatei f�r IgelUtils Version: " + this.getDescription().getVersion());
		this.getConfig().addDefault("broadcaster.enableBroadcaster", false);
		this.getConfig().addDefault("broadcaster.prefix", "&1[&9IgelUtils&1] &r");
		this.getConfig().addDefault("broadcast.prefix", "&1[&9IgelUtils&1] &r");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}

	@Override
	public void onDisable()
	{
		if(database.isConnected())
		{
			database.close();
		}
	}
}
