package de.cagigs.igelutils.commands;


import org.bitbucket.igelborstel.igelcore.commandHandlers.PlayerCommand;
import org.bitbucket.igelborstel.igelutils.IgelUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class AcceptCommand extends PlayerCommand 
{
	private IgelUtils plugin;

	public AcceptCommand(IgelUtils m) 
	{
		this.plugin = m;
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public boolean command(Player player, Command cmd, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("accept"))
		{
			if(args.length != 1)
			{
				return false;
			}
			if(player.hasPermission("igelutils.accept"))
			{
				PermissionUser permuser = PermissionsEx.getUser(player);
				boolean defaultUser = true;
				for(String group : permuser.getGroupNames())
				{
					if(!group.equals(this.plugin.getConfig().getString("rulesaccept.newbiegroup")))
					{
						defaultUser = false;
					}
				}
				if(!defaultUser)
				{
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', this.plugin.config.getConfig().getString("lang.accept.secondaccept")));
					return true;
				}
				else
				{
					if(!args[0].equals(this.plugin.getConfig().getString("rulesaccept.password")))
					{
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', args[0] + this.plugin.config.getConfig().getString("lang.accept.wrongPassword")));
						return true;
					}
					else
					{
						this.plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', player.getDisplayName() + this.plugin.config.getConfig().getString("lang.accept.acceptBroadcast")));
						String[] userGroups = {this.plugin.getConfig().getString("rulesaccept.promoteGroup")};
						permuser.setGroups(userGroups);
						//permissions.playerRemoveGroup(player, this.plugin.getConfig().getString("rulesaccept.newbiegroup"));
						//permissions.playerAddGroup(player, this.plugin.getConfig().getString("rulesaccept.promoteGroup"));
						player.sendMessage(plugin.config.getConfig().getString("lang.accept.personalMessage"));
						Location warpLoc = new Location(Bukkit.getServer().getWorld(this.plugin.getConfig().getString("rulesaccept.playertp.world")), this.plugin.getConfig().getDouble("rulesaccept.playertp.x"), this.plugin.getConfig().getDouble("rulesaccept.playertp.y"), this.plugin.getConfig().getDouble("rulesaccept.playertp.z"), ((float)this.plugin.getConfig().getDouble("rulesaccept.playertp.yaw")), ((float)this.plugin.getConfig().getDouble("rulesaccept.playertp.pitch")));
						player.teleport(warpLoc);
						return true;
					}
				}
			}
		}
		// TODO Auto-generated method stub
		return false;
	}
	

}
