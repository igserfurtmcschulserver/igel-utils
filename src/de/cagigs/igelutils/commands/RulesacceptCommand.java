package de.cagigs.igelutils.commands;

import org.bitbucket.igelborstel.igelcore.commandHandlers.PlayerCommand;
import org.bitbucket.igelborstel.igelutils.IgelUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class RulesacceptCommand extends PlayerCommand
{
	private IgelUtils plugin;
	
	public RulesacceptCommand(IgelUtils m) 
	{
		this.plugin = m;
	}

	@Override
	public boolean command(Player player, Command cmd, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("rulesaccept"))
		{
			if(args.length != 1)
			{
				return false;
			}
			if(args[0].equalsIgnoreCase("settp"))
			{
				if(player.hasPermission("rulesaccept.settp"))
				{
					double x, y, z, yaw, pitch;
					x = player.getLocation().getX();
					y = player.getLocation().getY();
					z = player.getLocation().getZ();
					yaw = player.getLocation().getYaw();
					pitch = player.getLocation().getPitch();
					String world = player.getLocation().getWorld().getName();
					this.setTpCfg(world, x, y, z, yaw, pitch);
					player.sendMessage("[" + this.plugin.getDescription().getName() + "] " + 
										this.plugin.config.getConfig().getString("lang.manage.settp"));
					return true;
				}
				return false;
			}
			else if(args[0].equalsIgnoreCase("tp"))
			{
				if(player.hasPermission("rulesaccept.tp"))
				{
					player.sendMessage("[" + this.plugin.getDescription().getName() + "] " + this.plugin.config.getConfig().getString("lang.manage.tp"));
					Location warpLoc = new Location(Bukkit.getServer().getWorld(this.plugin.getConfig().getString("rulesaccept.playertp.world")), this.plugin.getConfig().getDouble("rulesaccept.playertp.x"), this.plugin.getConfig().getDouble("rulesaccept.playertp.y"), this.plugin.getConfig().getDouble("rulesaccept.playertp.z"), ((float)this.plugin.getConfig().getDouble("rulesaccept.playertp.yaw")), ((float)this.plugin.getConfig().getDouble("rulesaccept.playertp.pitch")));
					player.teleport(warpLoc);
					return true;
				}
				return false;
			}
			return false;
		}
		
		// TODO Auto-generated method stub
		return false;
	}

	private void setTpCfg(String world, double x, double y, double z, double yaw, double pitch) 
	{
		this.plugin.getConfig().set("rulesaccept.playertp.x", x);
		this.plugin.getConfig().set("rulesaccept.playertp.y", y);
		this.plugin.getConfig().set("rulesaccept.playertp.z", z);
		this.plugin.getConfig().set("rulesaccept.playertp.world", world);
		this.plugin.getConfig().set("rulesaccept.playertp.yaw", yaw);
		this.plugin.getConfig().set("rulesaccept.playertp.pitch", pitch);
		this.plugin.saveConfig();
		this.plugin.reloadConfig();
		
	}

}
