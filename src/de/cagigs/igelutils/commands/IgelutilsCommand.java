package de.cagigs.igelutils.commands;

import org.bitbucket.igelborstel.igelutils.IgelUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class IgelutilsCommand implements CommandExecutor 
{
	IgelUtils pl;
	public IgelutilsCommand(IgelUtils m) 
	{
		this.pl = m;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("igelutils"))
		{
			if(sender.hasPermission("igelutils.cmd"))
			{
				sender.sendMessage("[" + pl.getDescription().getName() + "] Version: " + 
								   pl.getDescription().getVersion());
				return true;
			}
			return true;
		}	
		// TODO Auto-generated method stub
		return false;
	}
	
}
